from trytond.pool import Pool

from . import health_capt_sarmiento
from . import note

from .report import patient_notes_report
from .report import notes_report

from .wizard import export_patient_notes
from .wizard import export_notes



def register():
    Pool.register(
        health_capt_sarmiento.PatientAmbulatoryCare,
        health_capt_sarmiento.PatientData,
        note.Note,
        export_patient_notes.ExportPatientNotesStart,
        module='health_capt_sarmiento', type_='model')
    Pool.register(
        patient_notes_report.PatientNotes,
        notes_report.NotesReport,
        module='health_capt_sarmiento', type_='report')
    Pool.register(
        export_patient_notes.ExportPatientNotesWizard,
        export_notes.ExportPrescriptionNotesWizard,
        export_notes.ExportInpatientNotesWizard,
        export_notes.ExportLabTestNotesWizard,
        export_notes.ExportLabNotesWizard,
        export_notes.ExportImageRequestNotesWizard,
        export_notes.ExportImageResultNotesWizard,
        export_notes.ExportNursingRoundNotesWizard,
        export_notes.ExportNursingAmbulatoryNotesWizard,
        module='health_capt_sarmiento', type_='wizard')
