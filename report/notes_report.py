from trytond.pool import Pool
from trytond.report import Report


class NotesReport(Report):
    'Patient notes'
    __name__= 'gnuhealth.notes.report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        IrNote = pool.get('ir.note')

        context = super(NotesReport, cls).get_context(records, data, name)
        if name:
            ir_notes = IrNote.search([('resource', 'in', name['resources'])])
            context['records'] = ir_notes
        return context
