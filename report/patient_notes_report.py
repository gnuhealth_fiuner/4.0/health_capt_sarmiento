from trytond.pool import Pool
from trytond.report import Report


class PatientNotes(Report):
    'Patient notes'
    __name__= 'gnuhealth.patient.notes.report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        IrNote = pool.get('ir.note')
        Patient = pool.get('gnuhealth.patient')
        Prescription = pool.get('gnuhealth.prescription.order')
        Inpatient = pool.get('gnuhealth.inpatient.registration')
        LabRequest = pool.get('gnuhealth.patient.lab.test')
        LabTest = pool.get('gnuhealth.lab')
        ImageRequest = pool.get('gnuhealth.imaging.test.request')
        ImageResult = pool.get('gnuhealth.imaging.test.result')
        NursingRound = pool.get('gnuhealth.patient.rounding')
        NursingAmbulatory = pool.get('gnuhealth.patient.ambulatory_care')

        context = super(PatientNotes, cls).get_context(records, data, name)
        if name:
            patients = Patient.search([('id', 'in', name['patients'])])
            context['records'] = patients
            context['patient_notes'] = {}
            for patient in patients:
                resources = []
                context['patient_notes'][str(patient.id)] = {}
                if name['patient']:
                    resources += Patient.search([('id', '=', patient.id)])
                if name['prescription']:
                    resources += Prescription.search([('patient', '=', patient.id)])
                if name['inpatient']:
                    resources += Inpatient.search([('patient', '=', patient.id)])
                if name['lab_request']:
                    resources += LabRequest.search([('patient_id', '=', patient.id)])
                if name['lab_test']:
                    resources += LabTest.search([('patient', '=', patient.id)])
                if name['image_request']:
                    resources += ImageRequest.search([('patient', '=', patient.id)])
                if name['image_result']:
                    resources += ImageResult.search([('patient', '=', patient.id)])
                if name['nursing_round']:
                    resources += NursingRound.search([('name', '=', patient.id)])
                if name['nursing_ambulatory']:
                    resources += NursingAmbulatory.search([('patient', '=', patient.id)])
                notes = IrNote.search([('resource', 'in', resources)])
                context['patient_notes'][str(patient.id)] = notes
        return context
