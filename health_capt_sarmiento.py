from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval

class PatientAmbulatoryCare(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ambulatory_care'

    @staticmethod
    def default_health_professional():
        return None

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.health_professional.readonly=False
        cls.health_professional.required=True
        cls.health_professional.states['readonly'] = Eval('state') == 'done'


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    patient_notes = fields.Function(
        fields.Many2Many('ir.note', None, None,
            'Patient notes', readonly="1"),
        'get_patient_notes')

    def get_patient_notes(self, name=None):
        pool = Pool()
        IrNote = pool.get('ir.note')
        Patient = pool.get('gnuhealth.patient')
        Prescription = pool.get('gnuhealth.prescription.order')
        Inpatient = pool.get('gnuhealth.inpatient.registration')
        LabRequest = pool.get('gnuhealth.patient.lab.test')
        LabTest = pool.get('gnuhealth.lab')
        ImageRequest = pool.get('gnuhealth.imaging.test.request')
        ImageResult = pool.get('gnuhealth.imaging.test.result')
        NursingRound = pool.get('gnuhealth.patient.rounding')
        NursingAmbulatory = pool.get('gnuhealth.patient.ambulatory_care')
        notes = []

        patients = Patient.search([('id', '=', self.id)])
        prescriptions = Prescription.search([('patient', '=', self.id)])
        inpatient_registrations = Inpatient.search([('patient', '=', self.id)])
        lab_requests = LabRequest.search([('patient_id', '=', self.id)])
        lab_tests = LabTest.search([('patient', '=', self.id)])
        image_requests = ImageRequest.search([('patient', '=', self.id)])
        image_results = ImageResult.search([('patient', '=', self.id)])
        nursing_rounds = NursingRound.search([('name', '=', self.id)])
        nursing_ambulatory = NursingAmbulatory.search([('patient', '=', self.id)])

        resources = patients + prescriptions + inpatient_registrations + lab_requests + lab_tests \
            + image_requests + image_results + nursing_rounds +nursing_ambulatory
        if resources:
            notes = IrNote.search([('resource', 'in', resources)])
            return [note.id for note in notes]
        return []
