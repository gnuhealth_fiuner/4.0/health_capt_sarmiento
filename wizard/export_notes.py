from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateAction
from trytond.transaction import Transaction
from trytond.pool import Pool


class ExportNotesWizard():

    print_ = StateAction('health_capt_sarmiento.act_notes_report')

    def fill_data(self):
        print(Transaction().context)
        pool = Pool()
        active_ids = Transaction().context.get('active_ids')
        active_model = Transaction().context.get('active_model')
        Model = pool.get(active_model)
        records = Model.search([('id', 'in', active_ids)])
        return {'resources': [str(x) for x in records]}

    def do_print_(self, action):
        data =self.fill_data()
        return action, data


class ExportPrescriptionNotesWizard(Wizard, ExportNotesWizard):
    "Export Prescription Notes - Wizard"
    __name__ = 'gnuhealth.prescription.order.export_notes.wizard'

    start_state = 'print_'


class ExportInpatientNotesWizard(Wizard, ExportNotesWizard):
    "Export Patient Notes - Wizard"
    __name__ = 'gnuhealth.inpatient.registration.export_notes.wizard'

    start_state = 'print_'


class ExportLabTestNotesWizard(Wizard, ExportNotesWizard):
    "Export Lab Test - Wizard"
    __name__ = 'gnuhealth.patient.lab.test.export_notes.wizard'

    start_state = 'print_'


class ExportLabNotesWizard(Wizard, ExportNotesWizard):
    "Export Lab - Wizard"
    __name__ = 'gnuhealth.lab.export_notes.wizard'

    start_state = 'print_'


class ExportImageRequestNotesWizard(Wizard, ExportNotesWizard):
    "Export Imaging Request - Wizard"
    __name__ = 'gnuhealth.imaging.test.request.export_notes.wizard'

    start_state = 'print_'


class ExportImageResultNotesWizard(Wizard, ExportNotesWizard):
    "Export Imaging Result - Wizard"
    __name__ = 'gnuhealth.imaging.test.result.export_notes.wizard'

    start_state = 'print_'


class ExportNursingRoundNotesWizard(Wizard, ExportNotesWizard):
    "Export Nursing Round - Wizard"
    __name__ = 'gnuhealth.patient.rounding.export_notes.wizard'

    start_state = 'print_'


class ExportNursingAmbulatoryNotesWizard(Wizard, ExportNotesWizard):
    "Export Nursing Ambulatory - Wizard"
    __name__ = 'gnuhealth.patient.ambulatory_care.export_notes.wizard'

    start_state = 'print_'
