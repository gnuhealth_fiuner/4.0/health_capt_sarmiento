from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.wizard import Wizard, Button, StateView, StateAction
from trytond.pool import Pool

class ExportPatientNotesStart(ModelView):
    "Export Patient Notes - Start"
    __name__ = 'gnuhealth.patient.export_notes.start'

    patients = fields.Many2Many('gnuhealth.patient', None, None,
                               "Patients", readonly=True)
    patient = fields.Boolean("Patient", help="Print patient notes")
    prescription = fields.Boolean("Prescription",
                    help="Print prescription notes")
    inpatient = fields.Boolean("Inpatient", help="Print inpatient notes")
    lab_request = fields.Boolean("Lab Request",
                    help="Print lab request notes")
    lab_test = fields.Boolean("Lab Test", help="Print lab test notes")
    image_request = fields.Boolean("Image Request",
                    help="Print image request notes")
    image_result = fields.Boolean("Image result",
                    help="Print image result notes")
    nursing_round = fields.Boolean("Nursing Round",
                    help="Print nursing round notes")
    nursing_ambulatory = fields.Boolean("Nursing Ambulatory",
                    help="Print nursing ambulatory notes")

class ExportNotes():
    start = StateView('gnuhealth.patient.export_notes.start',
            'health_capt_sarmiento.export_patient_notes_start_form',[
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print Notes', 'print_', 'tryton-ok', default=True),
                ])

    print_ = StateAction('health_capt_sarmiento.act_patient_notes_report')
    def default_start(self, fields):
        active_ids = Transaction().context.get('active_ids')
        return {
            'patients': active_ids}

    def do_print_(self, action):
        data = {
            'patients': [x.id for x in self.start.patients],
            'patient': self.start.patient,
            'prescription': self.start.prescription,
            'inpatient': self.start.inpatient,
            'lab_request': self.start.lab_request,
            'lab_test': self.start.lab_test,
            'image_request': self.start.image_request,
            'image_result': self.start.image_result,
            'nursing_round': self.start.nursing_round,
            'nursing_ambulatory': self.start.nursing_ambulatory,
            }
        return action, data

class ExportPatientNotesWizard(Wizard, ExportNotes):
    "Export Patient Notes - Start"
    __name__ = 'gnuhealth.patient.export_notes.wizard'
