from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class Note(metaclass=PoolMeta):
    __name__ = 'ir.note'

    model_ = fields.Function(
            fields.Char('Model'),
            'on_change_with_model_')

    @fields.depends('resource')
    def on_change_with_model_(self, name=None):
        pool = Pool()
        Model = pool.get('ir.model')
        User = pool.get('res.user')

        transaction = Transaction()
        user = User(transaction.user)
        if self.resource:
            with transaction.set_context(language=user.language):
                model, = Model.search([('model', '=', self.resource.__name__)])
                return model.name
        return ''
